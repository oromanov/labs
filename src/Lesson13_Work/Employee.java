package Lesson13_Work;

public class Employee {
    String name;
    String position;
    int salary;

    public Employee () {
    }

    public Employee (String name) {
        this.name = name;
    }

    public Employee (String name, String position) {
        this.name = name;
        this.position =  position;
    }

    public  Employee (String name, String position, int salary) {
        this.name = name;
        this.position = position;
        this.salary =  salary;
    }

    public int getSalary() {
        return salary;
    }
    public void setSalary(int salary) {
        this.salary = salary;
    }
    public String getPosition() {
        return position;
    }
    public void setPosition(String position) {
        this.position = position;
    }
}

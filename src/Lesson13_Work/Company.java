package Lesson13_Work;

public class Company {
    String name;
    String address;
    String sphere;
    Employee employee;
    Employee [] employees;

    public Company () {
    }

    public Company (String name, String sphere) {
        this.name = name;
        this.sphere = sphere;
    }

    public Company (String name, Employee[] employees) {
        this.name = name;
        this.employees = employees;
    }

    public Company (String name, String address, String sphere, Employee[] employees) {
        this.name = name;
        this.address = address;
        this.sphere = sphere;
        this.employees = employees;
    }

    public Company (String name, String sphere, Employee[] employees) {
        this.name = name;
        this.sphere = sphere;
        this.employees = employees;
    }

    public void findEmployeeByPosition (String position) {
        for (Employee employee : employees) {
            if (employee.position.equals(position)) {
                System.out.println(employee.name);
            }
        }
    }

    private void findCompanyOwner () {
        for (Employee employee : employees) {
            if (employee.position.equals("owner")) {
            System.out.println(employee.name);
            }
        }
    }

    public void  findCeo () {
        for (Employee employee : employees) {
            if (employee.position.equals("CEO")) {
                System.out.println(employee.name);
               }
            }
        }

    public void setEmployees(Employee[] employees) {
        this.employees = employees;
    }

    public void calculateAverageSalary () {
                int sumSalary = 0;
                for (Employee employee : employees) {
                    sumSalary = +employee.salary;
                } System.out.println("средняя зарплата в компании "+ name + " " + sumSalary / employees.length);
            }


            public void calculateMaxSalary () {
                int maxSalary = 0;
                for (Employee employee : employees) {
                    if (employee.salary > maxSalary) {
                        maxSalary = employee.salary;
                    }
                } System.out.println("максимальная зарплата в компании "+ name+ " "+ maxSalary);
            }
        }
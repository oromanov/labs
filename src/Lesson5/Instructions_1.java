package Lesson5;

public class Instructions_1 {
    public static void main(String[] args) {

        System.out.println("Problem №1:");
        long randomVal = Math.round(Math.random() * 3);
        if (randomVal == 0) {
            System.out.println("Время года - зима");
        } else if (randomVal == 1) {
            System.out.println("Время года - весна");
        } else if (randomVal == 2) {
            System.out.println("Время года - осень");
        } else if (randomVal == 3) {
            System.out.println("Время года - лето");
        } else {
            System.out.println("Такое время года программе неизвестен");
        }

        System.out.println("Problem №2:");
        int a = 1;
        int b = 2;
        int c = 3;
        if ((a < b) && (b < c) == true) {
            System.out.println("Выражение истинно");
        } else {
            System.out.println("Выражение ложно");
        }

        System.out.println("Problem №3:");
        int i = 1;
        int j = 1;
        int m = 5;
        while (++i <= m) j *= i;
        System.out.println("Факториал " + m + " равен " + j);

        System.out.println("Problem №4:");
        int x = 1234567897;
        System.out.print("Перевернутое число: ");
        System.out.print(x % 10);
        while ((x /= 10) != 0)
            System.out.print(x % 10);
        System.out.println(" ");

        System.out.println("Problem №5:");
        int z = 0;
        for (int y = 0; y <= 9; y++) z += Math.pow(3, y);
        System.out.println("Среднее арифметическое: " + (z/10));

        System.out.println("Problem №6:");
        long n = 0;
        int length = 0;
        while ((n /= 10) != 0) length++;
        System.out.println("Количество цифр: "+ (length + 1));

        System.out.println("Problem №7:");
        int number = 1;
        int pow = 0;
        while (number % 2 == 0) {
          number /= 2;
          pow++;
        }
        if (number==1) {System.out.println("да, является - это 2 в степени "+ pow);}
        else System.out.println("не является степенью 2ки");

        System.out.println("Problem №8:");
        long adjective = Math.round(Math.random() * 9);
        long name_ = Math.round(Math.random() * 10);
        Integer adj = (int) (long) adjective;
        Integer name = (int) (long) name_;
        System.out.print("Ваше супергеройское имя: ");
        switch (adj)
        {
            case 0:
                System.out.print("РАДИОАКТИВНЫЙ ");
                break;
            case 1:
                System.out.print("ГРЕЧНЕВЫЙ ");
                break;
            case 2:
                System.out.print("ДЕМОНИЧЕСКИЙ ");
                break;
            case 3:
                System.out.print("ПРИЗРАЧНЫЙ ");
                break;
            case 4:
                System.out.print("ОЗОРНОЙ ");
                break;
            case 5:
                System.out.print("ПРЫЩАВЫЙ ");
                break;
            case 6:
                System.out.print("КОСМИЧЕСКИЙ ");
                break;
            case 7:
                System.out.print("ЗВЕЗДНЫЙ ");
                break;
            case 8:
                System.out.print("СЕКСУАЛЬНЫЙ ");
                break;
            case 9:
                System.out.print("НЕПОБЕДИМЫЙ ");
                break;
        }
        switch (name) {
            case 0:
                System.out.println("КАПИТАН");
                break;
            case 1:
                System.out.println("ЭЛЬФ");
                break;
            case 2:
                System.out.println("ПИНГВИН");
                break;
            case 3:
                System.out.println("ИНДЕЕЦ");
                break;
            case 4:
                System.out.println("ГНОМ");
                break;
            case 5:
                System.out.println("УПЫРЬ");
                break;
            case 6:
                System.out.println("БОРОДАЧ");
                break;
            case 7:
                System.out.println("БОБЕР");
                break;
            case 8:
                System.out.println("КОРОЛЬ");
                break;
            case 9:
                System.out.println("ТОЛСТОПУЗ");
                break;
            case 10:
                System.out.println("КИЛЛЕР");
                break;
        }
    }
}

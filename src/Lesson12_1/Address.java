package Lesson12_1;

public class Address {
    public String country;
    public String city;
    public String street;
    public int apartment;
    public long index;

    Address () {
    }

    public Address(String country) {
        this.country = country;
    }

    public Address (String country, String city, String street, int apartment, long index) {
        this.country = country;
        this.city = city;
        this.street = street;
        this.apartment = apartment;
        this.index = index;
    }

    public void printAddress () {
        if (country!= null)
            System.out.print(country + " ");
        if (city != null)
            System.out.print(city + " ");
        if (street != null)
            System.out.print(street + " ");
        if (apartment != 0)
            System.out.print(apartment + " ");
        if (index != 0)
            System.out.println(index + " ");
    }
}

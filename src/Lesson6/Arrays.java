package Lesson6;

public class Arrays {
    public static void main(String[] args) {
        System.out.println("Problem №1:");

        int[] problem1 = {2,4,6,8};
        System.out.print("Элементы первоначального массива: ");
        for (int i = 0; i <problem1.length; i++) {
            System.out.print(problem1[i]);
        }
        System.out.println(" ");

        int[] problem1_reverse = new int[4];
        for (int k=0; k <problem1_reverse.length; k++) {
           problem1_reverse[k]=problem1[problem1_reverse.length-k-1];
        }
        System.out.print("Элементы реверсивного массива: ");
        for (int l = 0; l <problem1_reverse.length; l++) {
            System.out.print(problem1_reverse[l]);
        }
        System.out.println(" ");

        System.out.println("Problem №2_Var1:"); //вариант если имелось в виду, что сами числа чётные
        int[] problem2 = {2,4,6,8,-8,16,1,2,-5,-10, 12, 60};
        int sum=0;
        int j=0;
        while (j<problem2.length) {
            if ((problem2[j]>0)&&(problem2[j] % 2 == 0)) sum += problem2[j];
            ++j;
        }
        System.out.println ("Сумма чётных положительных элементов вариант 1:" + sum);

        System.out.println("Problem №2_Var2:"); //вариант если имелось в виду, что номера элементов чётные. Причем нумерование элемонтов массива начинается с 0 (чётное)
        int sum_2=0;
        int j_2=0;
        while (j_2<problem2.length) {
            if ((problem2[j_2]>0)&&(j_2 % 2 == 0)) sum_2 += problem2[j_2];
            ++j_2;
        }
        System.out.println ("Сумма чётных положительных элементов вариант 2:" + sum_2);
    }
}

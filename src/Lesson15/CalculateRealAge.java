package Lesson15;

public class CalculateRealAge {
    static Integer ageConst = Integer.valueOf(7);
    public static void calculateAge(BasicCat cat) {
        System.out.println("Человеческий возраст кота " + cat.name + " " + cat.age*ageConst);
    }
}

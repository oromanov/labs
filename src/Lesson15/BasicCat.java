package Lesson15;

import java.util.Arrays;

public class BasicCat {
    String name;
    Double age;
    Double weight;
    static Integer numKittens = 0;
    static BasicCat[] cats = new BasicCat[1];
    static BasicCat[] catsCopy;

    public  BasicCat(String name, Double age, Double weight) {
        this.name = name;
        this.age = age;
        this.weight = weight;
        if (age<1) {
            numKittens++;
        }
       newCat(this);
    }

    public static void howManyKittens () {
        System.out.println("Число котят: " + numKittens.toString());
    }

    public static void newCat (BasicCat cat) {
        if (cats[0] == null) {
            cats[0] = cat;
        } else {
            catsCopy = new BasicCat[cats.length];
            catsCopy = Arrays.copyOf(cats, cats.length);
            cats = new BasicCat[catsCopy.length + 1];
            for (Integer i = 0; i < catsCopy.length; i++) {
                cats[i] = catsCopy[i];
            }
            cats[cats.length - 1] = cat;
        }
    }

     public static void quantityCats() {
            System.out.println("Количество созданных экземпляров (котов): " + cats.length);
        }
}

package Lesson13;

import Lesson13_Work.Company;
import Lesson13_Work.Employee;

public class HW {
    public static void main(String[] args) {
        Employee ownerTsys = new Employee("Jens", "owner");
        Employee ceoTsys = new Employee("Andreas", "CEO");
        Employee testerTsys = new Employee("Oksana");
        testerTsys.setPosition("tester");
        Employee ownerJetBrains = new Employee("Ralf", "owner");
        Employee ceoJetBrains = new Employee("Tomas", "CEO");
        Employee testerJetBrains = new Employee("Alina", "tester");
        Employee ownerDell = new Employee("Florian", "owner");
        Employee ceoDell = new Employee("Mario", "CEO");
        Employee testerDell = new Employee("Elena", "tester");
        testerTsys.setSalary(1500);
        testerJetBrains.setSalary(2000);
        testerDell.setSalary(1300);
        ownerTsys.setSalary(5000);
        ownerJetBrains.setSalary(4500);
        ownerDell.setSalary(6000);
        ceoTsys.setSalary(4700);
        ceoJetBrains.setSalary(4000);
        ceoDell.setSalary(4400);
        Employee[] t_systemsEmployees = {testerTsys, ownerTsys, ceoTsys};
        Employee[] jetBrainsEmployees = {testerJetBrains, ownerJetBrains, ownerJetBrains};
        Employee[] dellEmployees = {testerDell, ownerDell, ceoDell};
        Company t_systems = new Company ("T-Systems", "IT", t_systemsEmployees);
        Company jetBrains = new Company ("Jet Brains", "IT");
        Company dell = new Company ("Dell", "IT");
        jetBrains.setEmployees(jetBrainsEmployees);
        dell.setEmployees(dellEmployees);
        t_systems.findEmployeeByPosition("tester");
        t_systems.findCeo();
        t_systems.calculateAverageSalary();
        t_systems.calculateMaxSalary();
        jetBrains.calculateAverageSalary();
        jetBrains.calculateMaxSalary();
        dell.calculateAverageSalary();
        dell.calculateMaxSalary();
    }
}

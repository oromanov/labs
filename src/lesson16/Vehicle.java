package lesson16;

public class Vehicle {
    double mass;
    double enginePower;

    public Vehicle(double mass, double enginePower) {
        this.mass=mass;
        this.enginePower=enginePower;
    }
}

package lesson16;

import java.util.Calendar;

public class Doc {
    private byte doc;
    private Calendar date;
    private float size;

    public Doc(Calendar date, float size) {
        this.date=date;
        this.size=size;
    }

    private void setDoc(byte doc) {
        this.doc = doc;
    }

    private byte getDoc() {
        return doc;
    }
}

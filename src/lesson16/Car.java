package lesson16;

public class Car extends Vehicle {
    private int passengerSeats;
    private int numberOfPassengers;

    public Car (double mass, double enginePower, int passengerSeats) {
        super(mass, enginePower);
        this.passengerSeats = passengerSeats;
    }
    public void loadCar(int numberOfPassengers) {
        if (numberOfPassengers > passengerSeats) {
            System.out.println("Пассажиры не помещаются");
            numberOfPassengers=0;
        } else System.out.println("Пассажиры сели в машину");
        this.numberOfPassengers=numberOfPassengers;
    }

    public void unloadCar() {
        numberOfPassengers = 0;
        System.out.println("Пассажиры высажены");
    }
}

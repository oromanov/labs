package lesson16;

public class Truck extends Vehicle {
    private double carBodyVolume;
    private double load;

    public Truck(double mass, double enginePower, double carBodyVolume) {
        super(mass, enginePower);
        this.carBodyVolume = carBodyVolume;
    }

    public void loadTruck(double load) {
        if (load > carBodyVolume) {
            System.out.println("Груз не помещается");
            load = 0;
        } else System.out.println("Груз помещён в кузов");
        this.load=load;
    }

    public void unloadTruck() {
        load = 0;
        System.out.println("Кузов пуст");
    }
}

package lesson16;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class HW {
    public static void main(String[] args) {
        System.out.println("Problem №1");
        Calendar dateTex = new GregorianCalendar(2019, 11 , 22);
        Calendar dateBinary = new GregorianCalendar(2019, 11 , 23);
        TexDoc texDoc = new TexDoc(dateTex, 10, "Hello, world!");
        BinaryDoc binaryDoc = new BinaryDoc(dateBinary, 1, (byte) -128);//не понимаю, почему пришлось кастовать в байт, джава воспринимает как инт и всё тут!
        texDoc.print();
        binaryDoc.run();

        System.out.println("Problem №2");
        Truck truck = new Truck(150.8, 500.0, 90.6);
        Car car = new Car(80, 250, 4);
        TipTruck tipTruck = new TipTruck (171.4, 498.0, 120.3);
        truck.loadTruck(80.1);
        truck.unloadTruck();
        truck.loadTruck(197.0);
        car.loadCar(5);
        car.loadCar(4);
        car.unloadCar();
        tipTruck.carBodyDown();
        tipTruck.carBodyUp();
    }
}

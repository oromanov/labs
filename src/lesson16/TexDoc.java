package lesson16;

import java.util.Calendar;

public class TexDoc extends Doc {
    private String text;
    public TexDoc(Calendar date, float size, String text) {
        super(date,size);
        this.text=text;
    }
    public void print () {
        System.out.println("Текстовый документ отправлен на печать: "+text);
    }
}

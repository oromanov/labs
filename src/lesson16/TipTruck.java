package lesson16;

public class TipTruck extends Truck {
    public TipTruck(double mass, double enginePower, double carBodyVolume) {
        super(mass, enginePower, carBodyVolume);
    }

    public void carBodyUp() {
        System.out.println("Кузов поднят!");
    }

    public void carBodyDown() {
        System.out.println("Кузов опущен!");
    }
}

package Lesson4;

public class Instructions {
    public static void main(String[] args) {
        System.out.println("Problem №1:");
        System.out.println("Logical AND:");
        System.out.println("true & true: " + (true & true));
        System.out.println("false & true: " + (false & true));
        System.out.println("true & false: " + (true & false));
        System.out.println("false & false: " + (false & false));
        System.out.println("Logical OR:");
        System.out.println("true | true: " + (true | true));
        System.out.println("false | true: " + (false | true));
        System.out.println("true | false: " + (true | false));
        System.out.println("false | false: " + (false | false));
        System.out.println("Logical XOR:");
        System.out.println("true ^ true: " + (true ^ true));
        System.out.println("false ^ true: " + (false ^ true));
        System.out.println("true ^ false: " + (true ^ false));
        System.out.println("false ^ false: " + (false ^ false));
        System.out.println("Logical NOT:");
        System.out.println("!false: " + !false);
        System.out.println("!true: " + !true);
        System.out.println("Logical AND-NOT:");
        System.out.println("!(true & true): " + (!(true & true)));
        System.out.println("!(false & true): " + (!(false & true)));
        System.out.println("!(true & false): " + (!(true & false)));
        System.out.println("!(false & false): " + (!(false & false)));
        System.out.println("Logical OR-NOT:");
        System.out.println("!(true | true): " + (!(true | true)));
        System.out.println("!(false | true): " + (!(false | true)));
        System.out.println("!(true | false): " + (!(true | false)));
        System.out.println("!(false | false): " + (!(false | false)));

        System.out.println("Problem №2:");
        int a = 4;
        int b = 9;
        int c = 6;
        int d = 10;
        boolean result_2 = ((a % 2==0) & (b % 4==0)) || ((c % 3==0) & (d % 3!=0));
        System.out.println("Result of problem №2 " + (result_2));

        System.out.println("Problem №3:");//считаю, что биты нумеруются 0й, 1й, 2й. т.е. например в числе 10001010: 1й бит=1, 2й бит = 0
        int f = 201;
        System.out.println ("Result of problem №3 " + (((f & 4)==0) & ((f & 32)==0)&((~f&8)==0)&((~f&128)==0)|(f==100)|(f==500)|(f==0)));

        System.out.println("Problem №4:");
        int chislo = 513579;
        int zifra_chisla = 6;
        System.out.println("Result of problem №4 " + ((int)(chislo / (Math.pow(10, (--zifra_chisla)))))%10);
    }
}

package Lesson12;

import Lesson12_1.Address;

public class HW {
    public static void main(String[] args) {
        Address workAddress0 = new Address("USA");
        Address workAddress1 = new Address("USA","NY", "CentralPerk", 1, 11111);
        Address homeAddress = new Address("USA","NY", "MainRoad", 2, 222222);

        workAddress0.printAddress();
        System.out.println(" ");
        workAddress1.printAddress();
        homeAddress.printAddress();

        Person joseph = new Person ("Joseph", "Tribbiani", 30, homeAddress, workAddress0);
        Person phoebe = new Person ("Phoebe", "Tribbiani", 29, homeAddress, workAddress1);
        Person rachel = new Person ("Rachel", "Green", 30, homeAddress, workAddress1);

        joseph.addressWork.printAddress();
        phoebe.addressWork.printAddress();
        rachel.addressWork.printAddress();

        joseph.addressHome.printAddress();
        phoebe.addressHome.printAddress();
        rachel.addressHome.printAddress();
    }
}

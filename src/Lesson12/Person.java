package Lesson12;

import Lesson12_1.Address;

public class Person {
    String familyName;
    String givenName;
    int age;
    private Address addressHome;
    public Address addressWork;

    public Person () {
    }

    public Person (String familyName, String givenName) {
        this.familyName=familyName;
        this.givenName=givenName;
    }

    //Полная информация
    public Person (String familyName, String givenName, int age, Address addressHome, Address addressWork) {
        this.familyName=familyName;
        this.givenName=givenName;
        this.age=age;
        this.addressHome=addressHome;
        this.addressWork=addressWork;
    }
}

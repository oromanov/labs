package Lesson8;

public class Strings {
    public static void main(String[] args) {
        System.out.println("Problem №1:");
        System.out.println(politeMethod("Oksana"));

        System.out.println("Problem №2:");
        System.out.println(makeWrapWord("Crabs", "}}{{"));

        System.out.println("Problem №3:");
        System.out.println(half("It is a half of a string"));

        System.out.println("Problem №4:");
        System.out.println(endsOod("Have you understood"));

        System.out.println("Problem №5:");
        System.out.println(charCounter("Feeling good", 'e'));

        System.out.println("Problem №6:");
        System.out.println(oddLength(3.141592653));

        System.out.println("Problem №7:");
        System.out.println(stringCounter("Java programmers JavA love Java", "jAvA"));
    }

    //1) Напишите вежливую функцию, которая возвращает следующую строку:
//"Hello Alice !"
    public static String politeMethod(String name) {
        return "Hello " + name + "!";
    }

    //2) "Оберните" слово word в "обертку" wrap
// Например makeWrapWord("Hello", "(())") -> ((Hello))
//          makeWrapWord("Crabs", "}}{{") -> }}Crabs{{
//длина wrap = 4 всегда
    public static String makeWrapWord(String word, String wrap) {
        return "" + wrap.charAt(0) + wrap.charAt(1) + word + wrap.charAt(2) + wrap.charAt(3);
    }

    //3) Напишите функцию, которая возвращает первую половину строки
    public static String half(String halfstring) {
        return halfstring.substring(0, halfstring.length() / 2);
    }

    //4) Верните true, если строка оканчивается на "ood"
    public static boolean endsOod(String odd) {
        String yourstring = odd.substring(odd.length() - 3);
        String pattern = "ood";
        if (yourstring.equals(pattern))
            return true;
        else return false;
    }

    //5) Верните количество вхождений заданного символа в заданную строку
    public static int charCounter(String string, char symbol) {
        int i = 0;
        for (int j = 0; j < string.length(); j++) {
            if (string.charAt(j) == symbol)
                i++;
        }
        return i;
    }

    //6) Преобразуйте число в строку и верните true, если количество символов в строке нечетное
    public static boolean oddLength(double d) {
        String problem_6 = String.valueOf(d);
        if (problem_6.length() % 2 == 0)
            return false;
        else return true;
    }

    //7) Верните количество вхождений искомой строки в строке для поиска (игнорируя регистр)
//stringCounter("Java programmers love Java", "java") -> 2
    public static int stringCounter(String s, String searchString) {
        String x = s.toLowerCase();
        String y = searchString.toLowerCase();
        String z;
        int k = 0;
        while (x.length() >= y.length()) {
            if (x.endsWith(y)) {
                k++;
            }
            z = x.substring(0, x.length() - 1);
            x = z;}
            return k;
    }
}


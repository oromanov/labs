package lesson17;

import java.util.Date;

public class TimestampedReport extends Report {
    Date date = new Date(System.currentTimeMillis());

    public TimestampedReport(String report) {
        super(report);
    }

    @Override
    public String getReport () {
        return i + ": " + date+ ": " + report;
    }
}

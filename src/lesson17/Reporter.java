package lesson17;

public class Reporter {
    public static void main(String[] args) {
        Reporter reporter = new Reporter();
        Report report1 = new Report("Cool stuff"); //для абстрактным классо же нельзя создавать экземпляр, поэтому сделала Report не абстрактным
        reporter.report(report1); // не смогла придумать, как по-другому реализовать счетчик отчётов :( Подскажите?
        Report report2 = new TimestampedReport("Cool stuff2");
        reporter.report(report2);
        Report report3 = new UsernameReport("Cool stuff3");
        reporter.report(report3);
    }

    public void report(Report report){
        System.out.println("My current report: " + report.getReport());
    }
}

package Lesson3;

public class Data_types {
    public static void main(String[] args) {
        byte number_1 = -128;
        short number_2 = 32767;
        int number_3 = 68;
        long number_4 = 2222L;
        float floating_1 = 3.14F;
        double floating_2 = 1.1;
        char symbol = 'X';
        boolean logical = true;
        String string = "Lesson3";
        System.out.print(number_1 + " " + number_2 + " " + number_3 + " " + number_4 + " " + floating_1 + " " + floating_2 + " " + symbol + " " + logical + " " + string);
    }
}

package Lesson7;

import java.util.Scanner;

public class Functions {
    public static void main(String[] args) {
        System.out.println("Problem №1: ");
        long currentTime = System.nanoTime();
        System.out.println("Рекурсивным методом: " + Fibonachi_rekursion(8));
        long endtime_recursion = System.nanoTime();
        int a = 1;
        int b = 1;
        int Fibonachi_cycle = 1;
        int number = 8;
        if (number == 1 || number == 2) System.out.println("Методом цикла: " + 1);
        else {
            for (int k = 1; k <= number - 2; k++) {
                b = Fibonachi_cycle;
                Fibonachi_cycle += a;
                a = b;
            }
            System.out.println("Методом цикла: " + Fibonachi_cycle);
        }
        long endtime_cycle = System.nanoTime();
        System.out.println("Количество наносекунд, затраченное на функцию: " + (endtime_recursion - currentTime));
        System.out.println("Количество наносекунд, затраченное на цикл: " + (endtime_cycle - endtime_recursion));

        System.out.println("Problem №2: ");
        System.out.println("Пожалуйста введите число от 0 до 4");
        /*Spock = 0;
        Scissors = 1;
        Paper = 2;
        Rock = 3;
        Lizard = 4;*/
        Scanner scanner = new Scanner(System.in);
        int m;
        do {m = scanner.nextInt();
            int n = (int) Math.round(Math.random() * 4);
            System.out.println("Ваш выбор:");
            Val(m);
            System.out.println("Выбор компьютера:");
            Val(n);
            if (game(m, n) == m) System.out.println("Ура, Вы победили!");
            else {
                if (game(m, n) == 0) System.out.println("Ничья");
                else { if (m>=5) System.out.println("Введено неправильное число, невозможно определить исход игры");
                 else System.out.println("О нет! Вы проиграли!");
                }
            }
        } while (m!=9);
    }

    public static long Fibonachi_rekursion(long i) {
        if (i < 1) {
            System.out.print("Sam vinovat");
            return -1;
        }
        if (i == 1 || i == 2) {
            return 1;
        } else {
            return Fibonachi_rekursion(i - 2) + Fibonachi_rekursion(i - 1);
        }
    }

    public static int game(int m, int n) {
        if ((n - m == 1) || ((m == 4) && (n == 0))) {
            return m;
        } else {
            if (((m < 2) && ((n - m) == 3)) || ((m >= 2) && ((m - n) == 2))) return m;
            else {
                if (m == n) return 0;
                else return n;
            }
        }
    }

    public static void Val(int v) {
        if (v == 0) System.out.println("Spock");
        else {
            if (v == 1) System.out.println("Scissors");
            else {
                if (v == 2) System.out.println("Paper");
                else {
                    if (v == 3) System.out.println("Rock");
                    else {
                        if (v == 4) System.out.println("Lizard");
                        else System.out.println("Вы выбрали неверное число");
                    }
                }
            }
        }
    }
}
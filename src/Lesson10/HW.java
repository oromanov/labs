package Lesson10;

public class HW {
    public static void main(String[] args) {
        Point centre = new Point(0, 0);
        Circle myCircle = new Circle(1, centre);
        myCircle.moveCentre(2,3);
        myCircle.sCircle();
        myCircle.pCircle();
        Point topLeftCorner = new Point (0, 0);
        Square mySquare = new Square(5,topLeftCorner);
        mySquare.getBottomRightCornerSquare();
        mySquare.sSquare();
        mySquare.pSquare();
        Rectangle myRectangle = new Rectangle (6,7,topLeftCorner);
        myRectangle.getBottomRightCornerRectangle();
        myRectangle.sRectangle();
        myRectangle.pRectangle();
        mySquare.moveTopLeftCorner(-2,-3);
        mySquare.turnSquare(30);
        myRectangle.turnRectangle(90);
    }
}
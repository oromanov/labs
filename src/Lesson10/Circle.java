package Lesson10;

public class Circle {
    int radius;
    Point centre;
    double sCircle;
    double pCircle;

    public Circle (int radius, Point centre) {
        this.radius=radius;
        this.centre=centre;
    }

    public void sCircle () {
        sCircle = Math.PI * Math.pow(radius, 2);
        System.out.println ("Площадь круга =" + sCircle);
    }

    public void pCircle () {
        pCircle = 2*Math.PI * radius;
        System.out.println ("Периметр круга =" + pCircle);
    }

    public Point moveCentre (double a, double b) {
        return new Point (centre.x + a, centre.y+b);
    }
}

package Lesson10;

public class Square {
    int side;
    Point topLeftCorner;
    int sSquare;
    int pSquare;
    public Point getBottomRightCornerSquare () {
        return new Point (topLeftCorner.x + side, topLeftCorner.y -side);
    }
    public Square (int side, Point topLeftCorner) {
        this.side=side;
        this.topLeftCorner=topLeftCorner;
    }
    public void sSquare () {
        sSquare = (int)Math.pow(side, 2);
        System.out.println ("Площадь квадрата =" + sSquare);
    }
    public void pSquare () {
        pSquare = 4*side;
        System.out.println ("Периметр квадрата =" + pSquare);
    }
    public Point moveTopLeftCorner (double a, double b) {
        return new Point (topLeftCorner.x + a, topLeftCorner.y+b);
    }
    //Вращение вокруг верхней левой точки:
    // newX = centerX + (point2x-centerX)*Math.cos(x) - (point2y-centerY)*Math.sin(x);
    //newY = centerY + (point2x-centerX)*Math.sin(x) + (point2y-centerY)*Math.cos(x);
    // point 2 - нижняя правая точка
    public Point turnSquare (double angle) {
        return new Point ((topLeftCorner.x +side*Math.cos(angle/180 * Math.PI)-(-side)*Math.sin(angle/180 * Math.PI)), (topLeftCorner.y+ side*Math.sin(angle/180 * Math.PI)+(-side)*Math.cos(angle/180 * Math.PI)));
    }
}

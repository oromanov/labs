package Lesson10;

public class Rectangle {
    int height;
    int width;
    Point topLeftCorner;
    int sRectangle;
    int pRectangle;
    public Point getBottomRightCornerRectangle () {
        return new Point (topLeftCorner.x + width, topLeftCorner.y -height);
    }
    public Rectangle (int height, int width, Point topLeftCorner) {
        this.height=height;
        this.width=width;
        this.topLeftCorner=topLeftCorner;
    }
    public void sRectangle () {
        sRectangle = height*width;
        System.out.println ("Площадь прямоугольника =" + sRectangle);
    }
    public void pRectangle () {
        pRectangle = 2*(height+width);
        System.out.println ("Периметр прямоугольника =" + pRectangle);
    }
    public Point turnRectangle (double angle) {
        return new Point ((topLeftCorner.x +width*Math.cos(angle/180 * Math.PI)-(-height)*Math.sin(angle/180 * Math.PI)), (topLeftCorner.y+ width*Math.sin(angle/180 * Math.PI)+(-height)*Math.cos(angle/180 * Math.PI)));
    }
}
